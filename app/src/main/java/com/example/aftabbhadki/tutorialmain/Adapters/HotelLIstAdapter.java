package com.example.aftabbhadki.tutorialmain.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.aftabbhadki.tutorialmain.Db.DatabaseHandler;
import com.example.aftabbhadki.tutorialmain.Models.HotelListModel;
import com.example.aftabbhadki.tutorialmain.R;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Locale;

/**
 * Created by aftabbhadki on 12/30/17.
 */

public class HotelLIstAdapter extends RecyclerView.Adapter<HotelLIstAdapter.MyViewHolder> {

    private List<HotelListModel> hotelList;
    Context context;
    DatabaseHandler db;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView hotelname;
        public ImageView map,favourite,hotelimage;

        public MyViewHolder(View view) {
            super(view);
            hotelname = (TextView) view.findViewById(R.id.tv_hotelname);
            map = (ImageView) view.findViewById(R.id.iv_map);
            favourite = (ImageView) view.findViewById(R.id.iv_favourite);
            hotelimage = (ImageView) view.findViewById(R.id.iv_hotel);

        }
    }


    public HotelLIstAdapter(Context context, List<HotelListModel> hotelList) {
        this.hotelList = hotelList;
        this.context = context;
        db = new DatabaseHandler(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.hotellist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final HotelListModel hotels = hotelList.get(position);
        holder.hotelname.setText(hotels.getHotelname());
        Picasso.with(context)
                .load(hotels.getHotelurl())
                .into(holder.hotelimage);
        holder.map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                double latitude = hotels.getLatitude();
                double longitude = hotels.getLongitude();

                String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                context.startActivity(intent);

            }
        });
        holder.favourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                db.addContact(new HotelListModel(hotels.getId(),hotels.getHotelname(),hotels.getHotelurl(),
                        hotels.getLatitude(),hotels.getLongitude(),4.5));

            }
        });

    }

    @Override
    public int getItemCount() {
        return hotelList.size();
    }
}
