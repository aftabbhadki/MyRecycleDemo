package com.example.aftabbhadki.tutorialmain.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

import com.example.aftabbhadki.tutorialmain.Models.HotelListModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aftabbhadki on 12/30/17.
 */

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "hotellist";
    private static final String TABLE_CONTACTS = "hotels";
    private static final String HOTEl_ID = "id";
    private static final String HOTEL_NAME = "name";
    private static final String HOTEL_URL = "phone_number";
    private static final String HOTEL_LATITUDE = "latitude";
    private static final String HOTEL_LONGITUDE = "longitude";
    Context context;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
                + HOTEl_ID + " INTEGER PRIMARY KEY,"
                + HOTEL_NAME + " TEXT,"
                + HOTEL_URL + " TEXT,"
                + HOTEL_LATITUDE + " TEXT,"
                +HOTEL_LONGITUDE + " TEXT"+ ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new contact
   public void addContact(HotelListModel hotel) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(HOTEL_NAME, hotel.getHotelname()); // Contact Name
        values.put(HOTEL_URL, hotel.getHotelurl()); // Contact Phone
        values.put(HOTEL_LATITUDE, hotel.getLatitude()); // Contact Phone
        values.put(HOTEL_LONGITUDE, hotel.getLongitude()); // Contact Phone


        // Inserting Row
        db.insert(TABLE_CONTACTS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
        Log.d("Aftab","Added to FAvourites");
        Toast.makeText(context,"Added",Toast.LENGTH_SHORT).show();
    }

    // code to get the single contact
    HotelListModel getContact(String empNo) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT * FROM hotels WHERE name=?", new String[] {empNo + ""});
        if (cursor != null)
            cursor.moveToFirst();

        HotelListModel contact = new HotelListModel();
//                contact.setID(Integer.parseInt(cursor.getString(0)));
        contact.setHotelname(cursor.getString(1));
        contact.setHotelurl(cursor.getString(2));
        contact.setLatitude(Double.parseDouble(cursor.getString(3)));
        contact.setLongitude(Double.parseDouble(cursor.getString(4)));
        // return contact
        return contact;
    }

    // code to get all contacts in a list view
    public List<HotelListModel> getAllContacts() {
        List<HotelListModel> contactList = new ArrayList<HotelListModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                HotelListModel contact = new HotelListModel();
//                contact.setID(Integer.parseInt(cursor.getString(0)));
                contact.setHotelname(cursor.getString(1));
                contact.setHotelurl(cursor.getString(2));
                contact.setLatitude(Double.parseDouble(cursor.getString(3)));
                contact.setLongitude(Double.parseDouble(cursor.getString(4)));
                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

//    // code to update the single contact
//    public int updateContact(Contact contact) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, contact.getName());
//        values.put(KEY_PH_NO, contact.getPhoneNumber());
//
//        // updating row
//        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.getID()) });
//    }

//    // Deleting single contact
//    public void deleteContact(Contact contact) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete(TABLE_CONTACTS, KEY_ID + " = ?",
//                new String[] { String.valueOf(contact.getID()) });
//        db.close();
//    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }


}
