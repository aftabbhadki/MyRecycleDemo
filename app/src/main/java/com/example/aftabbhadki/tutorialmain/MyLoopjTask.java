package com.example.aftabbhadki.tutorialmain;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

/**
 * Created by aftabbhadki on 12/30/17.
 */

public class MyLoopjTask {
    private static final String TAG = "MOVIE_TRIVIA";

    AsyncHttpClient asyncHttpClient;
    RequestParams requestParams;
    Context context;
    OnLoopjCompleted onLoopjCompleted;

    String BASE_URL = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=22.3072,73.1812&radius=400&name=Vadodara&key=AIzaSyBMxSqACbLtQeTsu0d-2ExKIdfzVuxMPv8";
    String jsonResponse;

    public MyLoopjTask(Context context,OnLoopjCompleted onLoopjCompleted) {
        asyncHttpClient = new AsyncHttpClient();
        requestParams = new RequestParams();
        this.context = context;
        this.onLoopjCompleted = onLoopjCompleted;
    }

    public void executeLoopjCall(String queryTerm) {
        //requestParams.put("s", queryTerm);
        asyncHttpClient.get(BASE_URL, requestParams, new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                super.onSuccess(statusCode, headers, response);
                jsonResponse = response.toString();
                onLoopjCompleted.taskCompleted(jsonResponse);
                Log.i(TAG, "onSuccess: " + jsonResponse);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);
                Log.e(TAG, "onFailure: " + errorResponse);
            }
        });
    }

}
