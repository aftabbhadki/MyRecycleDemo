package com.example.aftabbhadki.tutorialmain.Ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.example.aftabbhadki.tutorialmain.Adapters.HotelLIstAdapter;
import com.example.aftabbhadki.tutorialmain.Models.HotelListModel;
import com.example.aftabbhadki.tutorialmain.MyLoopjTask;
import com.example.aftabbhadki.tutorialmain.OnLoopjCompleted;
import com.example.aftabbhadki.tutorialmain.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements OnLoopjCompleted {

    RecyclerView recyclerView;
    List<HotelListModel> hotelListModels = new ArrayList<>();
    HotelLIstAdapter hotelLIstAdapter;
    MyLoopjTask myLoopjTask;
    ImageView my_favourites;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        my_favourites = (ImageView) findViewById(R.id.my_favourites);
        my_favourites.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyFavouritesActivity.class);
                startActivity(intent);


            }
        });
        ////static data////
//        hotelListModels.add(new HotelListModel("11","Hotel!","",32.80035950000001,-96.82524649999999,4.5));
//        hotelListModels.add(new HotelListModel("11","Hotel!","",32.80035950000001,-96.82524649999999,4.5));
//        hotelListModels.add(new HotelListModel("11","Hotel!","",32.80035950000001,-96.82524649999999,4.5));
//        hotelListModels.add(new HotelListModel("11","Hotel!","",32.80035950000001,-96.82524649999999,4.5));
        hotelLIstAdapter = new HotelLIstAdapter(this,hotelListModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(hotelLIstAdapter);

        myLoopjTask = new MyLoopjTask(this,this);
        myLoopjTask.executeLoopjCall("");
    }

    @Override
    public void taskCompleted(String results) {

        Log.d("REsult",results);
        hotelListModels.clear();
        try {
            JSONObject jsonObject = new JSONObject(results.toString());
            JSONArray jsonArray = jsonObject.getJSONArray("results");

            for(int i=0;i<jsonArray.length();i++){

                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                JSONArray jsonArray1 = jsonObject1.getJSONArray("photos");

                ///name and photo path we can get here///////
                JSONObject jsonObject2 = jsonObject1.getJSONObject("geometry");
                JSONObject jsonObject3 = jsonObject2.getJSONObject("location");

                JSONObject jsonObject4 = jsonArray1.getJSONObject(0);
                //////latitide and longitude ///
                hotelListModels.add(new HotelListModel("1",jsonObject1.optString("name")
                        ,"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&" +
                        "photoreference="+jsonObject4.optString("photo_reference")+"&key=" +
                        "AIzaSyBMxSqACbLtQeTsu0d-2ExKIdfzVuxMPv8",jsonObject3.optDouble("lat"),jsonObject3.getDouble("lng")
                ,4.5));
                hotelLIstAdapter.notifyDataSetChanged();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
