package com.example.aftabbhadki.tutorialmain.Ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.aftabbhadki.tutorialmain.Adapters.HotelLIstAdapter;
import com.example.aftabbhadki.tutorialmain.Db.DatabaseHandler;
import com.example.aftabbhadki.tutorialmain.Models.HotelListModel;
import com.example.aftabbhadki.tutorialmain.R;

import java.util.ArrayList;
import java.util.List;

public class MyFavouritesActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<HotelListModel> hotelListModels = new ArrayList<>();
    HotelLIstAdapter hotelLIstAdapter;
    DatabaseHandler db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_favourites);
        db = new DatabaseHandler(this);
        recyclerView = (RecyclerView) findViewById(R.id.my_favourites);

        //static data////
        hotelListModels = db.getAllContacts();
        hotelLIstAdapter = new HotelLIstAdapter(this,hotelListModels);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(hotelLIstAdapter);


    }
}
