package com.example.aftabbhadki.tutorialmain.Models;

/**
 * Created by aftabbhadki on 12/30/17.
 */

public class HotelListModel {
    String id;
    String hotelname;
    String hotelurl;
    double latitude;
    double longitude;
    double ratings;

    public HotelListModel() {
    }

    public HotelListModel(String id, String hotelname, String hotelurl, double latitude, double longitude, double ratings) {
        this.id = id;
        this.hotelname = hotelname;
        this.hotelurl = hotelurl;
        this.latitude = latitude;
        this.longitude = longitude;
        this.ratings = ratings;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHotelname() {
        return hotelname;
    }

    public void setHotelname(String hotelname) {
        this.hotelname = hotelname;
    }

    public String getHotelurl() {
        return hotelurl;
    }

    public void setHotelurl(String hotelurl) {
        this.hotelurl = hotelurl;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getRatings() {
        return ratings;
    }

    public void setRatings(double ratings) {
        this.ratings = ratings;
    }
}
